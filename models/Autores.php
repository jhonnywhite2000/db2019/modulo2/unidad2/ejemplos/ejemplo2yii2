<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "autores".
 *
 * @property int $id
 * @property string $nombre
 * @property string $email
 * @property string $fechanacimiento
 *
 * @property Libros[] $libros
 */
class Autores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechanacimiento'], 'string', 'message'=>'La fecha de nacimiento no es valida'],
            [['nombre'], 'string', 'max' => 200],
            [['email'], 'email'],
            [['nombre', 'email'], 'required', 'message'=>'Es necesario rellenarlo']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Codigo del autor',
            'nombre' => 'Nombre completo del autor',
            'email' => 'Correo electronico',
            'fechanacimiento' => 'Fecha de nacimiento',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLibros()
    {
        return $this->hasMany(Libros::className(), ['autor' => 'id']);
    }
    public function afterFind(){
        parent::afterFind();
        
        $this->fechanacimiento=Yii::$app->formatter->asDate($this->fechanacimiento, 'php:d-m-Y'); // 2014-10-06
        }
        public function beforeSave($insert) {
            parent::beforeSave($insert);
            $this->fechanacimiento=Yii::$app->formatter->asDate($this->fechanacimiento, 'php:Y-m-d'); // 2014-10-06
            return true;
        }
    
    
    
}
