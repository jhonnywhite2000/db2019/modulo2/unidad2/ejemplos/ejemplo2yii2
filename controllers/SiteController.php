<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
    * @return string
     */
    public function actionIndex()
    {
       /* $a = Yii::$app
                ->db
                ->createCommand('select * from libros')->queryAll();
        echo "<pre>";
        var_dump($a);
        exit;*/
        return $this->render('index');
        
    }
    public function actionAutor()
    {   
       /* $id=50;
        $modelo = \app\models\Autores::find()->where("id=53")->one();
        if($modelo->load(Yii::$app->request->post()))
            $modelo->save();*/
        return $this->render("autores", [
            'model'=>$modelo,
        ]);
        
        
        /*$modelo = new \app\models\Autores();
        $modelo->load(Yii::$app->request->post());
        $modelo->save();
        return $this->render("autores", [
            'model'=>$modelo,
        ]);*/
        
        
        /*$modelo = new \app\models\Autores();
        $modelo->load(Yii::$app->request->post());
        echo "<pre>";
        var_dump($modelo);
        echo "</pre>";
        return $this->render("autores", [
            'model'=>$modelo,
        ]);*/
        
        
        
        /*echo "<pre>";
        var_dump(yii::$app->request->post());
        echo "</pre>";
        return $this->render("autores", [
            'model'=>new \app\models\Autores(),
        ]);*/
        
    }
    

    

    

    
}
